
ALL=$(wildcard *.md **/*.md)

all: $(ALL:.md=.html) tuiles #tuiles-missions/tuiles.html

.PHONY: tuiles

tuiles:
	cd tuiles-missions; python3 generate.py -n 1,2,3

%.html: %.md
	pandoc --mathjax -s $< -o $@ --css /style-md.css --template markdown-tpl.html
