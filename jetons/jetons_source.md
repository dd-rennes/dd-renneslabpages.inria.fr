---
title: Sources et données pour le jeu 'La juste tonne'
---

HBAB = How bad are bananas

**Questions sur les unités**
- Soit en poids / an
- Soit en pourcentage de l'empreinte objectif

# 1 paire de chaussures|10|shoes.svg
HBAB

# 1 AR au japon |4600|avion-sidney.png
HBAB

# 1 un nouveau laptop/5 ans|150|laptop-forge.png
**Source**: HBAB nous donne un range 200kg (low-cost)-800kg. Je pense que les ordinateurs de travail des  chercheurs inria ne sont pas low-cost, donc on est plus proche du haut.

# alimenter son laptop (8hrs/jrs)|4|laptop-forge.png
**Source**. Selon HBAB, un iMac, c'est 100W.
En France, on est à 30g/kWh ([Source](https://www.edf.fr/groupe-edf/agir-en-entreprise-responsable/rapports-et-indicateurs/emissions-de-gaz-a-effet-de-serre))
Pour 220 = (52-8)*5 jours ouvrés et 7h de travail:
154kWh consommés

# au boulot en voiture (20km/jr)|2000|car.png
**Source**. HBAB; 700g/mile pour une voiture moyenne.
  20km par jour ouvrable. (52-8) * 5 jours ouvrés = 220 jours
# nouvelle voiture (amortie en dix ans)|2000|car.png
**Source**. HBAB; Voiture moyenne 17T/an -- petite 6T
# burger (bœuf) 3jrs/semaine|312|burger.png

# La pinte d'IPA du vendredi soir|15|beer.svg
**Source**. HBAB nous donne 300g par pinte. J'ai multiplié par 52.
# Un café par jour|8|coffee.svg
**Source**. HBAB nous donne 21g par coupe. J'ai multiplié par 365.
# 100g de riz 5fois/semaine|80|riz.png
**Source**. HBAB nous donne 3kgCO₂/kg

# Electricité à la maison (+chauffage)|300|house.png
**Source**. Maison de 100m², 3 occupants en moyenne: 15 000kWh d'électricité par an.
Electricity mix: 70gCO₂/an

# 1AR Paris-Rennes (voiture)|173|car.png
**Source**: Calculateur de labo1.5
# 1AR Paris-Rennes (avion)|200|avion-sidney.png
**Source**: Calculateur de labo1.5

# 1AR Paris-Rennes (train)|4|train.svg
**Source**: Calculateur de labo1.5

# moyenne d'un.e français.e|10000|francais.png

# Accès au service public|1400|public.svg

# 4h/semaine de netflix|20|netflix.png
**Source**. [Article wired](https://www.wired.co.uk/article/netflix-carbon-footprint) qui donne 100g/h
# 4h/semaine de zoom|7|zoom.png
**Source**. [Article greenspector](https://greenspector.com/en/videoconferencing-apps-2022/) donne 33g/h.
