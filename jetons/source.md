---
title: Sources pour le jeu 'La juste tonne'
---

HBAB = How bad are bananas

**Questions sur les unités**
- Soit en poids / an
- Soit en pourcentage de l'empreinte objectif

### Vie de tous les jours
- *Une paire de chaussure par an*: 10kg
  **Source** HBAB
- *Une douche par jour*: 30kg-100kg
  **Source** HBAB entre 90g et 1.7kg (?) par douche. 1.7kg c'est une douche électrique sur la grille anglaise donc chez nous on doit rester en dessous de 200-300g pour des douches pas trop longues.
   - *moyenne d'un fraçais*: 10T. Sur [la page statistiques developpement durable du gouvernement](https://www.statistiques.developpement-durable.gouv.fr/lempreinte-carbone-de-la-france-de-1995-2021) on trouve 9T.
  
  
### Numérique

- *Travailler sur son ordinateur (électricité)*: 4kg [0.2%]
  **Source**. Selon HBAB, un iMac, c'est 100W.
  En France, on est à 30g/kWh ([Source](https://www.edf.fr/groupe-edf/agir-en-entreprise-responsable/rapports-et-indicateurs/emissions-de-gaz-a-effet-de-serre))
  Pour 220 = (52-8)*5 jours ouvrés et 7h de travail:
  154kWh consommés
- *Un nouvel ordinateur*: 500kg [25%]
  **Source**: HBAB nous donne un range 200kg (low-cost)-800kg. Je pense que les ordinateurs de travail des  chercheurs inria ne sont pas low-cost, donc on est plus proche du haut.
  
- *4h/semaine de netflix*: 20kg [1%].
  **Source**. [Article wired](https://www.wired.co.uk/article/netflix-carbon-footprint) qui donne 100g/h

- *4h/semaine de zoom*: 7kg/an [0.3%]
  **Source**. [Article greenspector](https://greenspector.com/en/videoconferencing-apps-2022/) donne 33g/h.
### Alimentation

- *100g de riz 5 fois par semaine*: 80kg [4%]
  **Source**. HBAB nous donne 3kgCO₂/kg

- *Un café par jour:* ~8kg/an [0.4%]
  **Source**. HBAB nous donne 21g par coupe. J'ai multiplié par 365.

- *La pinte d'IPA du vendredi soir*: 15kg/an [0.8%]
  **Source**. HBAB nous donne 300g par pinte. J'ai multiplié par 52.

- 100g de boeuf / 3fois par semaine: 312kg [15%]
  **Source**. HBAB nous donne 2kg par 100g
  
### Transport

### Avion
- 1AR Japon: 4.6T [225%]
  **Source:** HBAB
#### Voiture

- *Une nouvelle voiture, amortie sur dix ans:* moyenne 1.7T/an [85%]; petite 0.6T/an [30%]
  **Source**. HBAB; Voiture moyenne 17T/an -- petite 6T
- *Venir au labo en voiture (10km de distance)*: 2T/an [100%]
  **Source**. HBAB; 700g/mile pour une voiture moyenne.
  20km par jour ouvrable. (52-8) * 5 jours ouvrés = 220 jours

### Consommation électrique moyenne
- Logement : 300kg/an [13%]
  **Source**. Maison de 100m², 3 occupants en moyenne: 15 000kWh d'électricité par an.
  Electricity mix: 70gCO₂/an

### Trajets

Source: Labo 1.5

- 1AR Paris-Rennes (voiture): 173kg [8%]
- Paris-Rennes (avion): 200kg [10%]
- Paris-Rennes (TGV): 4kg [0.2%]

## Suggestions pour V2: Inria/IRISA
 - trajet pour mission (train/voiture/avion)
 - une nuit d'hotel
 - deplacement domicile-travail
 - gpu et cpu
 - visio
 - imprimer une thèse

