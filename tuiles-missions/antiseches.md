---
tags: DD, Inria, IRISA, 12 octobre, atelier, registration, workshop
title: Antisèches atelier du 12 octobre
---

## Exemples de mesures à prendre

### Mesures directes
- Mise en place de **quotas**
    - avec ou sans dépassement possible
    - avec ou sans pénalité si dépassement
    - reportable ou non sur période glissante (en général 3 ans) => ça permet d'anticiper des gros trajets
    - dépendant des statuts
    - à l'échelle individuelle, équipe, centre
- **Taxe Carbone** pour les trajets en avion
    - Montant de la taxe observé: 50 à 100€ par tonne
    - Le produit de la taxe pouvant servir à payer le surcoût actuel du train (voire payer le surclassement)
- **Report en train** des trajets inférieurs à Xh.
    - Avec ou sans surclassement
- Règles de priorité pour les jeunes chercheurs (Thésards/post-doc/jeunes CR)

### Mesures d'ordre 1 / Mesures indirectes


- Favoriser l'hybride 
- Organisation en hub continentaux
- Augmenter le temps minimum sur place
    - Coupler avec une visite d'équipe
- Montage de projets: prendre en compte la dimension environnementale dès la phase de rédaction que ce soit sur la logistique du projet (en particulier déplacements) et sur les impacts des résultats de recherche)
- Amélioration des outils de réservation (en particulier permettre la réservation multi-pays en train facilement)
- S'assurer que les outils de réservation donne une priorité aux trajets bas carbone
- Faciliter l'accès des expert.e.s en hybride de jurys (thèses ou autres)
- Limiter, pour un individu, le nombre d'implications simultanée dans les projets internationaux
- Atténuer les clauses de mobilités dans les recrutements
- Suivi de l'impact des missions au fil du temps
    - Par exemple intégré dans un bilan carbone
    - au niveau personnel / équipe / centre
- Sensibiliser
    - sur les consèquences de l'imaginaire positif liés aux événements internationaux en présentiel
    - sur tout autres sujet pertinents (wébinaires, ateliers, formations)
    - sur l'intérêt des conférences en mode hybride (au delà des inconvénients auxquels on pense d'abord) : participation plus large // moindre coût, égalité homme/femme ou jeunes parents, pas de problème de Visa. 
    - RETEX Bonnes pratiques conf hybrides ou virtuelles / GatherTown - Nous pouvons surement faire mieux encore. 

### Exemple
![](https://notes.inria.fr/uploads/upload_0485ae82e61708f49726e17d6323b860.png)


## Références
Ces élèments proviennent de quelques exemples votés au sein des laboratoires ou en cours d'évaluation... avec une grille de lecture assez perso (Matt) (source: https://cloud.le-pic.org/apps/onlyoffice/s/9J27xYnzHRybPsa?fileId=472507 c'est quelques docs aggrégés au sein du labos1point5 -- je ne sais pas si c'est ouvert).

## Ressources
* article Labo1p5 sur quotas : https://eartharxiv.org/repository/view/5995/
    
- Charte de bonnes pratiques / arbre de décisions
    - https://cloud.le-pic.org/s/9J27xYnzHRybPsa?dir=undefined&path=%2FDOCUMENTS_PARTAGES_RESEAU_TRANSITION1POINT5&openfile=472506
    - https://ns.inria.fr/sens/flyless/Flyless_poster_FR_06_2021.pdf
    - https://ethz.ch/content/dam/ethz/main/eth-zurich/nachhaltigkeit/Flugreisen-Projekt/Flight%20Decision%20Tree%20ETH%20Zurich_eng.pdf

    



