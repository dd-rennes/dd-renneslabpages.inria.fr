#! /usr/bin/python3
import sys, re, random, argparse

#########################
# TILE GENERATION SCRIPT
#########################

# Basic usage
#   python3 generate.py # Generate a single table
#   python3 generate.py -n $(seq -s ',' 4) # Generate four tables named 1, 2, 3 and 4

##############################################
# DATA PARAMETERS
##############################################


### CITIES ###################################

# Data tables for common destination
# Each city is mapped to a pair (footprint (in ton CO2e), frequency of visit)
footprint_table = {'ABU DHABI': (1.2, 5.0),
 'ATLANTA': (1.6, 1.0),
 'NAPLES': (0.32, 4.0),
 'BANGALORE': (1.8, 2.0),
 'BARCELONA': (0.2, 16.0),
 'BEIJING': (1.8, 3.0),
 'BOSTON': (1.2, 8.0),
 'BERLIN': (0.16, 8.0),
 'BUENOS AIRES': (2.5, 4.0),
 'DALLAS': (1.8, 10.0),
 'DELHI': (1.5, 8.0),
 'DETROIT': (1.4, 9.0),
 'DUBAI': (1.2, 4.0),
 'DUBLIN': (0.2, 5.0),
 'GENEVA': (0.2, 7.0),
 'HO CHI MINH CITY': (2.3, 2.0),
 'HONG KONG': (2.1, 3.0),
 'LIMA': (2.3, 1.0),
 'LOS ANGELES': (2.0, 12.0),
 'MINNEAPOLIS': (1.5, 3.0),
 'MONTREAL': (1.2, 12.0),
 'MUMBAI': (1.6, 4.0),
 'NEW YORK': (1.3, 9.0),
 'NEWARK': (1.3, 1.0),
 'SALT LAKE CITY': (1.8, 5.0),
 'SAN FRANCISCO': (2.0, 10.0),
 'SANTIAGO': (2.6, 4.0),
 'STOCKHOLM': (0.4, 10.0),
 'HELSINKI': (0.4, 1.0),
 'PRAGUE': (0.3, 2.0),
 'VIENNE': (0.35, 2.0),
 'PORTO': (0.17, 2.0),
 'SAO PAULO': (2.1, 4.0),
 'SEATTLE': (1.8, 3.0),
 'SINGAPORE': (2.4, 5.0),
 'TAIPEI': (2.2, 10.0),
 'TEL AVIV': (0.7, 3.0),
 'TOKYO': (2.2, 7.0),
 'TORONTO': (1.3, 6.0),
 'VANCOUVER': (1.8, 13.0),
 'WASHINGTON DC': (1.4, 9.0),
 'NICE': (0.2, 22.0),
 'SYDNEY': (3.6, 4.0)
}

### Description of the fictive team
team = {
    'PhD': { 'members': [
        {'name':'PhD #1', 'class': 'phd1'},
        {'name':'PhD #2', 'class': 'phd2'},
        {'name':'PhD #3', 'class': 'phd3'},
        {'name':'PhD #4', 'class': 'phd4'}],
             'footprint': 7.8,
             'number': 12},
    'rangA': { 'members': [{'name':'Prof/DR', 'class': 'dr'}],
               'footprint': 7.1,
               'number': 9},
    'technique': { 'members': [{'name':'Ingénieur·e', 'class': 'inge'}],
                   'footprint': 2.0,
                   'number': 3},
    'rangB': { 'members': [{'name':'McF/CR #1', 'class': 'cr1'}, {'name':'McF/CR #2', 'class': 'cr2'}],
               'footprint': 6.4,
               'number': 9},
    'postdoc': { 'members': [{'name':'Postdoc', 'class': 'postdoc'}],
                 'footprint': 1.62,
                 'number': 3 }
}


### Some predicates for motives
def is_permanent(m):
    return m['rank'] in ['rangA', 'rangB']

def is_cr(m):
    return m['rank'] in ['rangB']

def is_dr(m):
    return m['rank'] in ['rangA']

def is_temp(m):
    return not (m['rank'] in ['rangA', 'rangB'])
def is_phd(m):
    return (m['rank'] in ['PhD'])

def far(m):
    return m['footprint'] >= 1.

### List of reasons for traveling
reason_table = [
    ["Conférence annuelle A* (sans exposé)", 4],
    ["Conférence annuelle A* (avec exposé)", 4],
    ["Conférence annuelle A (sans exposé)", 3],
    ["Conférence annuelle A (avec exposé)", 3],
    ["Conférence annuelle B (sans exposé)", 2],
    ["Conférence annuelle B (avec exposé)", 2],
    ["Participation à un workshop", 3],
    [["Keynote dans LA conférence", is_permanent], 2],
    [["Accompagner le doctorant à sa première conf", is_permanent], 3],
    ["Participation à un workshop", 5],
    [["Jury soutenance de thèse", is_dr], 3],
    [["Jury soutenance de thèse", is_cr], 1],
    [["Exposé invité", is_permanent], 3],
    [["Jury de sélection", is_dr], 3],
    [["Jury de sélection", is_cr], 1],
    [["Visite de travail d'un mois", far], 1],
    [["Visite de travail de six mois", far], 1],
    [["Visite du labo en co-tutelle", lambda m: far(m) and is_phd(m)], 1],
    [["Réunion de consortium d'un projet", is_permanent], 2],
    [["Réunion de montage de projet", is_permanent], 2],
    [["École d'été d'une semaine", is_phd], 4]
]

################################
# TYPESETTING PARAMATERS

# Height of tiles (in cm)
tile_height = 10.5

# Number of lines
total_line_number = 6

# List of papers to generate to, by increasing line length.
papers = [
    { 'name': 'A4',
      'length': 29.5 },
    { 'name': 'A3',
      'length': 42. }
]

# We can now compute the main parameter, the "cm per ton"
# i.e. how much CO² does one centimeter of the width of a tile represent?
# The constraints is that the biggest tile needs to be representable
cm_per_ton = max (paper['length'] for paper in papers) / max ([footprint_table[x][0] for x in footprint_table])

def rank_of_person(person):
    """Given the name of a person, look up its rank in the team description"""
    for x in team:
        if person in team[x]['members']:
            return x
    return 'unknown: ' +person

mission_id = 0
def make_id():
    """Generate a fresh mission ID"""
    global mission_id
    mission_id += 1
    return mission_id


def make_mission (person, city, motive):
    """Create a mission from person, city, motive, by filling up the other fields"""
    return { 'person': person,
             'city': city,
             'footprint': footprint_table[city.upper()][0],
             'id': make_id(),
             'motive': motive,
             'rank': rank_of_person(person) }

def html_of_mission(mission, cm_per_ton):
    """HTML format a mission"""
    div_class = mission['rank']
    length = cm_per_ton * mission['footprint']
    if length < 3:
        div_class += ' verysmall'
    elif length < 10:
        div_class += ' small'
    elif length < 20:
        div_class += ' big'
    else:
        div_class += ' verybig big'
    if mission['table_name']:
        table = f"""<span class="table-name"><span>{mission['table_name']}</span></span>"""
    else:
        table = ""
        
    return f"""<div style="--tile-height: {tile_height}cm; --length: {length}cm" class="outer-tuile {div_class}">
               <div class="tuile-inner {div_class}">
               <span class="header">
                  <span class="where">{mission['city']}</span>
                  <span class="person {mission["person"]["class"]}">{mission['person']['name']}</span>
               </span>
               <span class="descr">{mission['motive']}</span>
               {table}
               <span class="reverse">
                  <span class="where">{mission['city']}</span>
                  <span class="person {mission["person"]["class"]}">{mission['person']['name']}</span>
               </span>
    
               </div></div>"""
#    return f"""<div style="--tile-height: {tile_height}cm; --length: {length}cm" class="outer-tuile {div_class}">
#    </div>"""
        

### Generation
def draw(list):
    """Draw an element from a weighted list"""
    return random.choices( population=[l[0] for l in list], weights=[l[1] for l in list], k=1)[0]

def draw_motive(person, city):
    """Draw a motive consistent with person and city. Return the corresponding mission."""
    while True:
        # Draw a motive from the table
        motive = draw(reason_table)
        # motives can either be strings (i.e. motive applicable to all contexts)
        # or pairs [motive, f] where f is a predicate
        if type(motive) == str:
            return make_mission(person, city, motive)
        else:
            mission = make_mission(person, city, motive[0])
            # Check the built mission against the predicate
            if motive[1](mission):
                return mission

def draw_mission(rank, allowed_footprint):
    """Draw a mission for the given rank, within the allowed remaining footprint"""
    person = draw ([ [x, 1.] for x in team[rank]['members'] ])
    cities = [ [city, frequency] for city, (footprint, frequency) in footprint_table.items() if footprint < allowed_footprint]
    city = draw(cities)
    mission = draw_motive(person, city)
    return mission

def generate_missions_for_rank(rank, table_name=None):
    """Generate all the missions for a given rank"""
    missions = []
    error = 100
    # Loop until we find a drawing that is within 5% in terms of emissions
    while error > 0.05:
        missions = []
        target_footprint = team[rank]['footprint']
        footprint_left = target_footprint
        for i in range(team[rank]['number']):
            m = draw_mission(rank, max(footprint_left, 0.4))
            m['table_name'] = table_name
            missions.append(m)
            footprint_left -= m['footprint']
        error = abs(footprint_left) / target_footprint
    return missions

def generate_missions(table_name=None):
    """Generate the missions for the team."""
    missions = []
    for rank in team:
        missions += generate_missions_for_rank(rank, table_name)
    return missions

def pave_missions(input_missions, line_size, max_lines=None):
    """Reorder missinons to ensure a good paving when exported to html.
If max_lines is an integer, enter "pretty paving mode". This will force
    (1) all lines to match at the end by modifying the footprints
    (2) Stop after max_lines lines.

    (This mode should only be used to create visuals)
    """
    paved_missions = []
    ton_per_line = line_size / cm_per_ton

    current_line_length = 0.
    current_line_tiles = []
    missions = list(input_missions)
    while missions != []:
        rest = ton_per_line - current_line_length
        candidates = [ m for m in missions if m['footprint'] < rest]
        if candidates == [] and current_line_length > 0.:
            current_line_length = 0.
            if max_lines:
                current_line_tiles[len(current_line_tiles)-1]['footprint'] += rest
            paved_missions.append(current_line_tiles)
            current_line_tiles = []
            if max_lines and len(paved_missions) >= max_lines:
                break
            else:
                continue
        if candidates == []:
            m = missions.pop()
            paved_missions.append([m])
        else:
            m = max(candidates, key = lambda m: m['footprint'])
            missions.remove(m)
            current_line_tiles.append(m)
            current_line_length += m['footprint']
    if current_line_tiles != []:
        paved_missions.append(current_line_tiles)
    return paved_missions

def pave_missions_by_lines(input_missions, line_number):
    """Pave missions on a number of lines"""

    lines = [ [] for i in range(line_number) ]
    while input_missions != []:
        m = max(input_missions, key = lambda m: m['footprint'])
        input_missions.remove(m)
        # Find shorter line
        min = sum([x['footprint'] for x in lines[0]])
        index = 0
        for i in range(len(lines)-1):
            s = sum([x['footprint'] for x in lines[i+1]])
            if s < min:
                min = s
                index = i+1
        lines[index].append(m)
    return lines
        
                
        
### Parsing from a file
def missions(mission_file):
    """Parse the mission of a file"""
    handle = open(mission_file, "r")
    lines = handle.readlines()
    handle.close()
    missions = []
    dict = {}
    for line in lines:
        result = re.search(r"(.*): *(.*)", line)
        if result:
            dict[result[1]] = result[2]
        if line == '---\n':
            missions.append(make_mission(dict['person'], dict['city'], dict['motive']))
            dict = {}
    if dict != {}:
        missions.append(make_mission(dict['person'], dict['city'], dict['motive']))
    return missions

def total_emissions_by_status(missions):
    """Compute the total emisions by rank"""
    result = {rank: 0 for rank in team}
    for m in missions:
        result[m["rank"]] += m["footprint"]
    return result

def max_column_width (lines):
    return max ([sum([m['footprint'] for m in line]) for line in lines])

def output_missions(lines, width, handle, bounding_box=False, visual=False, stats=True):
    """visual: do we want to create a pretty picture?"""
    handle.write(
        """
    <!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style.css" />
        </style>
        </head>
        <body>""")

    jetons_class = "jetons"
    if visual:
        jetons_class += " visual"
    if bounding_box:
        jetons_class += " jetons-bounded"
    handle.write(f"""
    <div style="--jetons-width: {width}cm; --jetons-height:{total_line_number * tile_height}cm" class="{jetons_class}">
    """)
    if bounding_box or visual:
        height = total_line_number * tile_height
        handle.write(f"""<div class=first style="--height: {height}cm; --width: {width * 0.707}cm"><span>-70%<span></div>""")
        handle.write(f"""<div class=second style="--height: {height}cm; --width: {width * 0.5}cm">><span>-50%</span></div>""")
    for line in lines:
        for mission in line:
            handle.write(html_of_mission(mission, cm_per_ton))
        handle.write('<br />\n')
    handle.write("</div>")

    handle.write("\n</body>\n</html>")

def write_prints(all_missions, filename):
    min_size = 0.
    remaining_missions = list(all_missions)
    print(len(remaining_missions))
    last = 0.
    for paper in papers:
        missions = []
        max_size = paper['length'] / cm_per_ton
        missions = [m for m in all_missions if last < m['footprint'] <= max_size]
        print(f"Writing {len(missions)} missions to {paper['name']+filename}")
        with open(paper['name'] + filename, 'w') as f:
            lines = pave_missions(missions, paper['length'])
            output_missions(lines, paper['length'], f, bounding_box=False, stats=False)
        last = max_size
    

    
def write_stats(tables, handle):
    handle.write("""    <!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style.css" />
        </style>
        </head>
        <body>""")

    handle.write('<h1>General stats</h1>\n')
    handle.write(f'<ul><li>Tile height: {tile_height}cm</li>')
    handle.write(f'<li>Line number: {total_line_number}</li>')
    handle.write(f'<li>Cm per T: {cm_per_ton}cm/T</li></ul>')

    for missions in tables:
        handle.write(f'<h1>Table #{missions[0]["table_name"]}</h1>')
        total_co2 = sum([m['footprint'] for m in missions])
        paved = pave_missions_by_lines(missions, total_line_number)
        footprint = [[m ['footprint'] for m in line] for line in paved]
        width = cm_per_ton * max([sum([m['footprint'] for m in line]) for line in paved])
        surface = width * total_line_number * tile_height
        actual_surface = tile_height * total_co2 * cm_per_ton
        handle.write(
            f'''<ul>
            <li>Total CO²: {total_co2}T</li>
            <li>Max width: {width}cm</li>
            <li>Wasted space: {int(100 - 100 * actual_surface/surface)}%</li>
                </ul>''')
        handle.write('<div class=visu>')
        output_missions(paved, width, f, bounding_box=True)
        handle.write('</div>')
        
    

parser = argparse.ArgumentParser( prog=__file__, formatter_class=argparse.RawDescriptionHelpFormatter, description="""Generate tiles for the dd-rennes workshop""")
parser.add_argument('-t', '--tiles', help='Path to the tiles file (the file to print).', default='tiles_print.html')
parser.add_argument('-p', '--preview', help='Path to the preview file (to preview a team).', default='preview.html')
parser.add_argument('-v', '--visual', help='Path to the visual file (pretty image).', default='visual.html')
parser.add_argument('-n', '--number', help='Number (or list of numbers) of tables to generate.', default='1')

args = parser.parse_args()

table_names = args.number.split(',')

tables = [ generate_missions(table_name=name) for name in table_names ]

all_the_missions = [mission for table in tables for mission in table]

write_prints(all_the_missions, args.tiles)

with open(args.preview, 'w') as f:
    write_stats(tables, f)

# total_emissions = sum([m["footprint"] for m in ms])
# line_emissions = total_emissions / total_line_number
# line_width = cm_per_ton * line_emissions

# with open(args.preview, 'w') as f:
#     output_missions(ms, line_width, f, bounding_box=True, stats=True)

# total_line_number = 4
# line_width *= 6/4
# with open(args.visual, 'w') as f:
#     output_missions(ms, line_width, f, visual=True, bounding_box=True, stats=False)
