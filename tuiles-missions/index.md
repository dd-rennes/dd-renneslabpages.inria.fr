---
title: Atelier « Quelles missions garder? »
---

*Résumé.* L'atelier « Quelles missions garder? » est un atelier qui permet à ses
participants d'imaginer une trajectoire de réduction des émissions
liées aux déplacements profesionnels. L'atelier a pour but de:

- Montrer aussi fidèlement que possible la réalité des pratiques de voyages au sein d'un centre ;
- Faire prendre conscience de la diversité des pratiques de voyage au sein d'un laboratoire ;
- Imaginer ensemble des mesures de réduction et d'évaluer leur impact.

# Principe de l'atelier
Dans cet atelier, chaque participant est invité à réfléchir à des
mesures de réduction de l'empreinte des missions. L'atelier est
découpé en deux phases : une étude de cas concrète en petit groupe et
une seconde étape de discussion collective pour construire ensemble
des mesures.


## Première phase : étude de cas en groupe de 5
Dans cette phase, les participants sont réunis en table de 5. Sur la
table est présentée le bilan des missions d'une équipe fictive sur une
période de trois ans [^trois-ans] sous une forme qui rend apparente
l'empreinte relative de chaque mission:

[^trois-ans]: Une période de trois ans permet de mieux lisser les
    déplacements, notamment pour les étudiants en thèse qui ne se
    déplacement pas forcément en moyenne tous les ans.

![](tuiles.png)

La trajectoire choisie dans l'atelier par l'équipe fictive est celle
d'atteindre -50% d'ici 2030 et d'atteindre ce but en deux étapes
successives de -30% (2023-2026, 2027-2029). Par table de cinq, les
participants devront débattre de quelle tuile garder ou enlever pour
que les tuiles choisies rentrent dans 70% (puis 50%) de la surface
initiale:

![](visuel.png)

A la table, un animateur prend des notes sur les tuiles enlevées. Dans
un second temps il s'agit d'identifier des principes (possiblement
encore flou à ce stade) qui justifie la suppression de la tuile :
mission jugée inutile, ou report (vers le train ou en visio) jugé
possible.

Chaque table a un ensemble de tuiles différent, qui cherche à
représenter la diversité des pratiques. Cela permet ainsi d'éviter
d'avoir des règles liés à une particularité du jeu de données.

## Seconde phase : discussion collective à 15
Dans un second temps, les participants de plusieurs tables sont
regroupés ensemble afin de confronter leur règle et de consolider des
propositions communes. Il s'agit aussi de confronter les réalités
rencontrées et de faire remonter des règles, des systèmes qui
permettent de formaliser les principes écrits dans la première phase.

Les animateurs prennent des notes sur les discussions et les mesures
proposées, leur popularité au sein du groupe, ainsi que les
limitations et compensations potentielles de chaque mesure.


# Déroulé approxmatif (3h)

1. **Introduction à l'atelier** (15minutes) puis répartition des participants en table de 5
2. **Première phase** (60m) en groupe de 5
   - Explication des objectifs et prise de connaissance des tuiles (20min)
   - Première manche: -30% (20min)
   - Seconde manche: -50% (20min)
3. **Pause** (15m)
4. **Seconde phase** (60m) en groupe de 15
   - Restitution de chaque groupe (3 x 5 minutes)
   - Discussion sur les points communs et les différences. Réflexion
     de règles à divers niveaux : individuel, équipe, institutionnel
5. **Conclusion**	 

Le déroulé précis de l'atelier à l'IRISA est disponible [ici](deroule_atelier.html).

# Données nécessaire à la conception des tuiles

## Mode réel

Il est possible de générer un ensemble de tuiles à partir d'un
ensemble de mission réel. Il faut dans ce cas pour chaque mission : le
missionnaire, la raison de son transport, ainsi que la ville.

## Mode génération aléatoire

Dans ce mode, les tuiles sont générées aléatoirement à partir d'un
modèle des missions du labo ou de l'unité considérée. Dans ce cas, il faut les données suivantes :

- Une liste de statut d'intérêt. *Exemple*: Rang A, Rang B, Thésard, Postdoc, Ingénieur.
- Le nombre de membres de chaque statut.
- Pour chaque statut, l'empreinte carbone de toutes les missions
  faites par des personnels de ce statut sur une période d'un an.
- (Optionnel) Pour chaque ville destination d'au moins une mission :
  le nombre de missions dans cette ville, et si possible l'empreinte
  carbone d'un voyage vers cet ville selon la méthodologie de l'outil
  que vous avez utiliser au point précédent.
- Une liste de raisons de voyages ainsi que pour chaque raison : un
  poids et potentiellement une contrainte (par exemple "seulement
  permanents"). *Exemple*: Conférence A*, 10, pas de contraintes. Jury
  de thèse, 3, seulement permanents. École d'été, 5, seulement
  thésard.
  

# Méthodologie des tuiles

## Conception de l'équipe fictive

Il faut d'abord constituer une équipe fictive dont les proportions
sont représentatives du labo étudié. Selon la granularité des données
de missions, distinguer certains statuts. Dans notre cas nous avons
distingué : Prof/DR, McF/CR, ingénieur, postdoc, et thésard. Nous
avons fixé 1 chef d'équipe Prof/DR et avons calculé le reste en
fonction de la proportion des agents du labo. Par exemple, chez nous,
il y a à peu près deux fois plus de McF/CF que de Prof/DR, quatre fois
plus de thésards que de Prof/DR etc.

## Missions par statut

Il est nécessaire aussi de calculer le nombre moyen de missions en
avion et leur empreinte totale pour un membre de chaque statut de
l'équipe fictive.

## Raison des motifs

Il faut aussi compiler une liste de raisons pour ces missions,
pondérées par leur fréquence.


# Générateur de tuiles

## Aspect de génération aléatoire 

Les tuiles sont générées aléatoirement selon certaines contraintes.

- *Description de l'équipe fictive.* Pour chaque statut, on donne le
  nombre de membres de ce statut dans l'équipe fictive, le nombre de
  missions effectués par tous les membres de ce statut ainsi que
  l'empreinte totale.

- *Description des destinations.* Nous avons une liste de ville étiquettées avec (1) l'empreinte
  carbone pour s'y rendre et (2) la fréquence de visites basés sur le
  nombre de missions dans cette ville sur un certain laps de temps
  (chez nous l'année 2019).

- *Description des motifs.* Une liste de raison de se déplacer
  (conférence, séjour de travail, workshop, ...) pondéré et
  potentiellement avec des contraintes (Seuls les permanents font des
  missions de type « Jury de thèse »).
  
Notre générateur génère ensuite un jeu de suite satisfaisant les
contraintes de nombres de missions et d'empreinte (à 5% près).

A noter que : 
- les missions sont générées par statut ce qui peut créer des
  déséquilibres : par exemple, si on dit qu'un thésard fait une
  mission par an, et que l'équipe comporte quatre thésards, il n'est
  pas forcé que chaque thésard aura bien une mission, mais il y aura
  quatre missions allouées aléatoirement aux quatre thésards.

- le générateur ne s'occupe pas de la période étudiée, il faut donc
  lui donner directement le nombre de missions et leurs empreintes sur
  la période désirée (dans notre cas: 3 ans).

# Manufacture du jeu de tuiles

## Manufacture des tuiles

Le générateur est appelé avec la liste des tables à générer et créé
deux fichiers `A4tuiles.html` et `A3tuiles.html` qui sont à imprimer
en `A4` et `A3` respectivement. Ces fichiers contiennent les tuiles de
toutes les tables. Après impression, il faut découper (par exemple au
massicot) les tuiles et les classer par table.

## Manufacture des feuilles

Pour marquer l'espace de jeu et les trajectoires, nous avons fabriqué
des feuilles pour organiser les tuiles. Par défaut les tuiles sont
réparties en 6 lignes de 10.5cm dont la longueur dépend de l'empreinte
exacte de la table (aux alentour de 50cm chez nous). Le générateur
indique la longueur de chaque table pour assurer un pavage
confortable, mais il faut faire attention car à l'impression il peut y
avoir un problème entre les centimètres virtuels et les centimètres
réels, ce qui nécessite d'ajuster la longueur indiquée par un facteur
de correction. 

Il faut ensuite découper un rectangle de la taille donnée pour chaque
table, à partir de vieux posters, feuilles paperboard ou ce que vous
avez sous la main et tracer deux traits verticaux, un à 50% et un à
70%.

