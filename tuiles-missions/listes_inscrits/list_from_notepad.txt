1. ocan.sankur@irisa.fr, SUMO, CR, French
2. nicolas.waldburger@irisa.fr, SUMO, PhD student, French
3. gerardo.rubino@inria.fr, ERMINE, DR, French
4. ghina.dandachi@inria.fr, ERMINE, Postdoc, French
5. olivier.ridoux@irisa.fr, DRUID, PR, French
6. sebastien.ferre@irisa.fr, LACODAM, PR, French
7. boris.clenet@inria.fr, EMPENN, Engineer, French
8. francesca.galassi@irisa.fr, EMPENN, MCF, French + English (+ Italian)
9. armelle.mozziconacci@irisa.fr, EMPENN, AER, French
10. constance.bocquillon@irisa.fr, EMPENN, Phd Student, FR-EN
11. marc.mace@irisa.fr, HYBRID, CR, French
12. guillaume.lomet@inria.fr, TARAN, PhD student, French + English
13. angeliki.kritikakou@irisa.fr, TARAN, MCF, French + English
14. benjamin.ninassi@inria.fr, DGD-S, Engineer, French
15. madeline.montigny@inria.fr, learning lab, Engineer, French
16. laurence.farhi@inria.fr, learning lab, Engineer, French
17. aurelie.lagarrigue@inria.fr, learning lab, Engineer, French
18. jean-loup.hatchikian-houdot@inria.fr, EPICURE, PhD student, French
19. stephanie.gosselin-lemaile@inria.fr,SAER, assistante,FR
21. marie.le_roic@inria.fr, SAER, assistante, french (+english)
22. Caroline.tanguy@inria.fr, SAER, assistante, FR+Spanish+English
23. mathias.malandain@inria.fr, SED, Engineer, FR+EN
24. alexandre.sanchez@inria.fr, SED, Engineer, FR+EN+Spanish
25. gilles.tissot@inria.fr, ODYSSEY, CR, French + English
26. igor.maingonnat@inria.fr, ODYSSEY, Phd student, French
27. charles.kervrann@inria.fr, SAIRPICO, DR, French
28. edith.blin@inria.fr, chargée Communication, SCM, French
29. monica.le-bezvoet@inria.fr, CPPI startups, STIP, French/English 
30. sara-sadat.hoseininasab@inria.fr, PACAP, PhD student, English
31. damien.marion@irisa.fr, CAPSULE, MCF*, FR+EN+ESP
32. teddy.furon@inria.fr, LinkMedia, DR, French 
33. timothe.albouy@irisa.fr, WIDE, PhD student, French
34. claudio.pacchierotti@irisa.fr, RAINBOW, CR, FR+EN+IT (need to leave a bit before the end)
35. burhan-rashid.hussein@inria.fr, EMPENN, Postdoc, English
36. marco.tognon@inria.fr, RAINBOW, ISFP, FR+EN+IT
37. arthur.rauch@inria.fr, WIDE, PhD student, FR/EN
38. benoit.caillaud@inria.fr, Hycomes, DR, FR+EN
39. christophe.droz@inria.fr, I4S, ISFP, FR-EN
40. Laurent.Mevel@inria.fr, I4S, DR, FR-EN
41. caroline.collange@inria.fr, PACAP, CR, FR/EN
42. emile.savalle@inria.fr, HYBRID, PhD Student, French + English
43. aline.roumy@inria.fr, SIROCCO, DR, French
44. remi.piau@inria.fr, SIROCCO, PhD, French
45. francois.lemercier@irisa.fr, MAGELLAN, MCF, French + English
46. noe.robert@inria.fr, DYLISS, IE, French + English
47. remi.cambuzat@inria.fr, VIRTUS, Engineer, French + English
48. aurore.alcolei@inria.fr, EPICURE, Engineer, French + English
49. francois.tessier@inria.fr, KERDATA, Researcher, French + English (9h-11h)
50. romain.lefeuvre@inria.fr, DiverSE, PhD Student, French + English
51. camille.muller@inria.fr, EMPENN, Postdoc, French + English
