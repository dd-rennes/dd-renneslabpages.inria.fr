# Pétri-Turing
### Table 1: Thomas Maugey - Pétri-Turing

- Olivier Ridoux
- Charles Kervrann
- Laurent Mevel
- Benoit Caillaud
- Teddy Furon

### Table 2: Francois Taiani (english) - Pétri-Turing

- Sara-Sadat Hoseininasab
- Burhan-Rashid Hussein
- Camille Muller
- Angeliki Kritikakou
- Marco Tognon

### Table 3: Simon Castellan - Pétri-Turing

- Nicolas Waldburger
- Constance Bocquillon
- Timothe Albouy
- Remi Piau
- Jean-Loup Hatchikian-Houdot

### Table 4: Matthieu Simonin puis Francesca Galassi - Pétri-Turing

- Gerardo Rubino
- Caroline Tanguy
- Aurore Alcolei
- Francois Tessier
- Romain Lefeuvre
- *Lydie Mabil (ou table 10, ou remplacement)*

### Table 5: Laurent Garnier - Pétri-Turing

- Marie Le Roic
- Remi Cambuzat
- Christophe Droz
- Emile Savalle
- Monica Le-Bezvoet



### Table 10: Matthieu Simonin - Petri-Turing

- Lydie Mabil (ou table 4)
- Julien Monnot (ou table 7)
- Frédéric Cérou (ou table 9)
- Romane Libouban (ou table 8)

### Table 11: ("spectateurs") - Petri-Turing

- Benjamin Ninassi
- Madeline Montigny
- Laurence Farhi
- Aurelie Lagarrigue

# Markov

### Table 6: Steven Derrien - Markov

- Ocan Sankur
- Marc Mace
- Gilles Tissot
- Claudio Pacchierotti
- Caroline Collange

### Table 7: Antoine L'Azou - Markov

- Aline Roumy
- Stephanie Gosselin-Lemaile
- Alexandre Sanchez
- Francois Lemercier
- Arthur Rauch

# Salle de Direction

### Table 8: Elise Bannier - salle de direction

- Sebastien Ferre
- Armelle Mozziconacci
- Ghina Dandachi
- Igor Maingonnat
- Mathias Malandain

### Table 9: Thomas Prampart - salle de direction

- Edith Blin
- Noe Robert
- Damien Marion
- Guillaume Lomet
- Boris Clenet
